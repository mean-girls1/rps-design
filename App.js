import React, {useState} from 'react';
import {StyleSheet, View, ActivityIndicator} from 'react-native';
import TokenContext from "./src/context/TokenContext";
import Routes from "./src/routes/Routes";

export default function App() {
    const [isLoading, setLoading] = useState(true);

    return (
        <TokenContext isLoading={setLoading}>
            <View style={styles.container}>
                {isLoading ? <View style={{marginTop: '50%'}}>
                    <ActivityIndicator size='large' color='pink'/>
                </View> : (<Routes/>)}
            </View>
        </TokenContext>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333',
    },
});
