import {StyleSheet} from "react-native";

export const SelectGameStyling = StyleSheet.create({
    startButton: {
        paddingHorizontal: 40,
        paddingVertical: 30,
        marginBottom: 5,
        marginLeft: '15%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ed3492',
        backgroundColor: '#ffe6ff',
        width: '70%'
    },
    buttonText: {
        color: '#ed3492',
        fontSize: 30,
        textAlign: 'center'
    },
    joinButton: {
        paddingHorizontal: 40,
        paddingVertical: 30,
        marginBottom: 30,
        marginLeft: '15%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#DDB207',
        backgroundColor: '#ffffcc',
        width: '70%'
    },
    majsButtonText: {
        color: '#DDB207',
        fontSize: 30,
        textAlign: 'center'
    }
})
