import React from "react";
import LogoBackground from "../startPage/components/LogoBackground";
import SelectGameButtons from "./components/SelectGameButtons";

export default function SelectGame() {
    return (
        <>
            <LogoBackground/>
            <SelectGameButtons/>
        </>
    )
}
