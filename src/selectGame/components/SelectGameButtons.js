import React, {useContext} from "react";
import {TouchableOpacity, Text, View} from "react-native";
import {useNavigation} from "@react-navigation/native";
import ApiHandler from "../../fetch/ApiHandler";
import {TokenContext} from "../../context/TokenContext";
import {SelectGameStyling} from "../selectGameStyling/SelectGameStyling";

const SelectGameButtons = () => {
    const myNavigation = useNavigation();
    const token = useContext(TokenContext);
    const startGame = (token) => {
        ApiHandler.startGame(token)
            .then(() => {
                myNavigation.navigate('Game');
            });
    }
    return (
        <View>
            <TouchableOpacity
                style={SelectGameStyling.startButton}
                onPress={() => {
                    startGame(token)
                }}
            >
                <Text style={SelectGameStyling.buttonText}>Start Game</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={SelectGameStyling.joinButton}
                onPress={() => {
                    myNavigation.navigate('Game list')
                }}
            >
                <Text style={SelectGameStyling.majsButtonText}>Join Game</Text>
            </TouchableOpacity>
        </View>
    )
}

export default SelectGameButtons;
