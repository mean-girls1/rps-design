import React, {useContext, useEffect, useState} from "react";
import {Text, View} from "react-native";
import ApiHandler from "../../fetch/ApiHandler";
import {TokenContext} from "../../context/TokenContext";
import {GameStyling} from "../gameStyling/GameStyling";

const NamesAndResults = () => {
    const token = useContext(TokenContext);
    const [game, setGame] = useState('');

    useEffect(() => {
        const interval = setInterval(() => {
            ApiHandler.getGameStatus(token)
                .then((gameStatus) => {
                    setGame(gameStatus);
                    console.log("Game= " + JSON.stringify(gameStatus));
                })
                .catch((error) => console.error(error))
        }, 100);
        return () => clearInterval(interval);
    }, []);

    const ResultText = () => {
        return (
            <View >
                <Text style={GameStyling.result}>{game.gameStatus === 'WIN' ?
                    'You WIN!' : game.gameStatus === 'LOSE' ?
                        'You LOSE..' : game.gameStatus === 'DRAW' ?
                            'Its a DRAW :P' : 'Waiting..'}</Text>
            </View>
        )
    };

    return (
        <View style={GameStyling.bigContainer}>
            <View style={GameStyling.container}>
                <Text style={GameStyling.playerName}>{game.name}</Text>
                <Text style={GameStyling.opponentName}>{game.opponentName}</Text>
            </View>
            <ResultText/>
        </View>
    )
}

export default NamesAndResults;

