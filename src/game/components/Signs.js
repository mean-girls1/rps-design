import React, {useContext} from "react";
import {Image, View, TouchableOpacity} from "react-native";
import ApiHandler from "../../fetch/ApiHandler";
import {TokenContext} from "../../context/TokenContext";
import {GameStyling} from "../gameStyling/GameStyling";

export default function Signs() {
    const token = useContext(TokenContext);

    const makeMove = (move) => {
        ApiHandler.getMove(token, move)
            .catch((error) => console.error(error))
        return(move);
    };

    return (
        <View>
            <View style={GameStyling.topSigns}>
                <TouchableOpacity
                    onPress={() => makeMove('ROCK') }>
                <Image
                    style={GameStyling.size}
                    source={require("../../../images/ROCK.png")}
                />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => makeMove('SCISSORS') }>
                <Image
                    style={GameStyling.size}
                    source={require("../../../images/SCISSORS.png")}
                />
                </TouchableOpacity>
            </View>
            <View style={GameStyling.bottomSigns}>
                <TouchableOpacity
                    onPress={() => makeMove('PAPER') }>
                <Image
                    style={GameStyling.size}
                    source={require("../../../images/PAPER.png")}
                />
                </TouchableOpacity>
            </View>
        </View>
    )
}


