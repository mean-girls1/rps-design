import {StyleSheet} from "react-native";

export const GameStyling = StyleSheet.create({
    container: {
        marginTop: '20%',
        flexDirection: 'row',
        justifyContent: 'space-around',

    },
    playerName: {
        color: '#f6afbf',
        fontSize: 25,
        fontWeight: 'bold'
    },
    opponentName: {
        color: '#a8cd88',
        fontSize: 25,
        fontWeight: 'bold'
    },
    scoreContainer: {
        justifyContent: 'center',
        flexDirection: 'row',
        fontSize: 100,
        marginTop: '10%'

    },
    bigContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    result: {
        color: '#fad163',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: '10%'
    },
    topSigns: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: '15%',
    },
    size: {
        height: 150,
        width: 150
    },
    bottomSigns: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: '20%',
    }
})
