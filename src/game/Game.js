import React from 'react';
import NamesAndResults from "./components/NamesAndResults";
import Signs from "./components/Signs";
export default function Game(){
    return (
        <>
        <NamesAndResults/>
        <Signs/>
        </>
    )
}
