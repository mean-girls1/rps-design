import React from "react";
import {TextInput, View} from "react-native";
import {StartPageStyling} from "../startPageStyling/StartPageStyling";

const InsertName = ({onChangeText, value}) => {
    return (
        <View>
            <TextInput
                style={StartPageStyling.input}
                onChangeText={onChangeText}
                value={value}
                placeholder="Type your name"
            />
        </View>
    );
}


export default InsertName;
