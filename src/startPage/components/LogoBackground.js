import React from "react";
import {Image, View} from "react-native";
import {StatusBar} from "expo-status-bar";

const LogoBackground = () => {
    return (
        <View style={{marginBottom: 40}}>
            <Image
                style={{height: 210, width: 145, marginLeft: '32%', marginTop: '20%'}}
                source={require("../../../images/rps-logo.png")}
            />
            <StatusBar style="auto"/>
        </View>
    );
}
export default LogoBackground;
