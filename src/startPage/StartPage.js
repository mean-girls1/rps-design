import React, {useContext, useState} from "react";
import {Text, TouchableOpacity, View} from "react-native";
import LogoBackground from "./components/LogoBackground";
import InsertName from "./components/InsertName";
import {TokenContext} from "../context/TokenContext";
import ApiHandler from "../fetch/ApiHandler";
import {StartPageStyling} from "./startPageStyling/StartPageStyling";

export default function StartPage({navigation}) {
    const [name, setName] = useState('');
    const token = useContext(TokenContext);

    const sendNameToServerAndStartGame = (name) => {
        ApiHandler.updateName(token, name)
            .then(response => {
                JSON.stringify(response);
                navigation.navigate('SelectGame')
            });
    }

    return (
        <View>
            <LogoBackground/>
            <InsertName onChangeText={setName} value={name}/>
            <TouchableOpacity
                style={name !== '' ? StartPageStyling.startButton : StartPageStyling.disabledButton}
                disabled={name === ''}
                onPress={() => {
                    sendNameToServerAndStartGame(name)
                }}
            >
                <Text style={name !== '' ? StartPageStyling.buttonText : StartPageStyling.disabledButtonText}>
                    Start Playing
                </Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={StartPageStyling.majsButton}
                onPress={() => {
                    sendNameToServerAndStartGame('Majs')
                }}
            >
                <Text style={StartPageStyling.majsButtonText}>Play as Majs 🌽</Text>
            </TouchableOpacity>
        </View>
    )
}

