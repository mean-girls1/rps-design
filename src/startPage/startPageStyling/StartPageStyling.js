import {StyleSheet} from "react-native";

export const StartPageStyling = StyleSheet.create({
    input: {
        height: 60,
        marginLeft: 60,
        borderWidth: 1,
        borderBottomWidth: 0,
        borderRadius: 10,
        paddingHorizontal: 20,
        borderColor: '#00cccc',
        color: '#00cccc',
        backgroundColor: '#e6ffff',
        width: '70%',
        fontSize: 20,
        textAlign: 'center'
    },
    disabledButton: {
        paddingHorizontal: 40,
        paddingVertical: 30,
        marginBottom: 5,
        marginLeft: '15%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'rgba(237,52,146,0.27)',
        backgroundColor: 'rgba(255,230,255,0.22)',
        width: '70%'
    },
    disabledButtonText: {
            color: 'rgba(237,52,146,0.2)',
            fontSize: 28,
            textAlign: 'center'
    },
    startButton: {
        paddingHorizontal: 40,
        paddingVertical: 30,
        marginBottom: 5,
        marginLeft: '15%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ed3492',
        backgroundColor: '#ffe6ff',
        width: '70%'
    },
    buttonText: {
        color: '#ed3492',
        fontSize: 28,
        textAlign: 'center'
    },
    majsButton: {
        paddingHorizontal: 40,
        paddingVertical: 30,
        marginBottom: 25,
        marginLeft: '15%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#DDB207',
        backgroundColor: '#ffffcc',
        width: '70%'
    },
    majsButtonText: {
        color: '#DDB207',
        fontSize: 28,
        textAlign: 'center'
    }
});
