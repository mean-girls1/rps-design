import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import SelectGame from '../selectGame/SelectGame';
import Game from "../game/Game";
import StartPage from "../startPage/StartPage";
import GameList from "../joinGame/GameList";

const Routes = createStackNavigator();

export default function Navigator() {
    return (
        <NavigationContainer>
            <Routes.Navigator>
                <Routes.Screen
                    name="Start Page"
                    component={StartPage}
                    options={{
                        headerStyle: {
                            backgroundColor: '#ffffcc',
                        },
                        headerTintColor: '#DDB207',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },
                    }}/>
                <Routes.Screen
                    name="SelectGame"
                    component={SelectGame}
                    options={{
                        headerStyle: {
                            backgroundColor: '#ffffcc',
                        },
                        headerTintColor: '#DDB207',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },
                    }}
                />
                <Routes.Screen
                    name="Game"
                    component={Game}
                    options={{
                        headerStyle: {
                            backgroundColor: '#ffffcc',
                        },
                        headerTintColor: '#DDB207',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },
                    }}
                />
                <Routes.Screen
                    name="Game list"
                    component={GameList}
                    options={{
                        headerStyle: {
                            backgroundColor: '#ffffcc',
                        },
                        headerTintColor: '#DDB207',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },
                    }}
                />
            </Routes.Navigator>
        </NavigationContainer>
    );
}
