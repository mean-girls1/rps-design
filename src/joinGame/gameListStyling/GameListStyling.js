import {StyleSheet} from "react-native";

export const GameListStyling = StyleSheet.create({
        container: {
            flex: 1,
        },
        logo: {
            marginTop: '5%',
            marginLeft: '10%'
        },
        list: {
            marginTop: 20,
            marginBottom: '20%',
            height: '60%'
        },
        item: {
            flex: 1,
            backgroundColor: '#ffe6ff',
            padding: 20,
            marginHorizontal: 16,
            borderBottomWidth: 1,
            borderColor: '#ed3492',
            borderRadius: 15
        },
        itemText: {
            flex: 1,
            textAlign: 'center',
            fontSize: 20,
            color: '#ed3492'
        }
    }
)
