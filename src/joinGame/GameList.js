import React, {useContext, useEffect, useState} from "react";
import {Image, View, FlatList, Text, TouchableOpacity} from "react-native";
import ApiHandler from "../fetch/ApiHandler";
import {TokenContext} from "../context/TokenContext";
import {GameListStyling} from "./gameListStyling/GameListStyling";

export default function GameList({navigation}) {
    const token = useContext(TokenContext);
    const [games, setGames] = useState([]);

    useEffect(() => {
        ApiHandler.getGameList(token)
            .then((game) => {
                setGames(game)
            })
            .catch((error) => console.error(error))
    }, []);

    const joinGameAndStart = (gameId) => {
        ApiHandler.joinGame(token, gameId)
            .then(response => {
                console.log("Clicked Game: ",
                    JSON.stringify(response));
                navigation.navigate('Game')
            });
    };

    return (
        <View style={GameListStyling.container}>
            <Image
                style={GameListStyling.logo}
                source={require("../../images/cuter-logo-listpage.png")}
            />
            <View style={GameListStyling.list}>
                <FlatList
                    style={GameListStyling.test}
                    data={games}
                    keyExtractor={({id}) => id}
                    renderItem={({item}) => (
                        <TouchableOpacity
                            onPress={() => {
                                joinGameAndStart(item.id)
                            }}
                        >
                            <View style={GameListStyling.item}>
                                <Text style={GameListStyling.itemText}>{item.name}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </View>
        </View>
    )
}
