import React, {createContext, useEffect, useState} from 'react';
import ApiHandler from "../fetch/ApiHandler";

export const TokenContext = createContext(null);

export default ({children, isLoading}) => {
    const [token, setToken] = useState('');

    useEffect(() => {
        ApiHandler.fetchToken()
            .then((newToken) => {
                setToken(newToken)
            })
            .catch((error) => console.error(error))
            .finally(() => isLoading(false));
    }, []);

    return (
        <TokenContext.Provider value={token}>
            {children}
        </TokenContext.Provider>
    )
}
