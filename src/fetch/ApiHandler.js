import React from "react";

const ApiHandler = {

    fetchToken() {
        return fetch("http://localhost:8080/auth/token")
            .then((response) => response.text()
                .then(text => text)
            );
    },

    updateName(token, name) {
        return fetch("http://localhost:8080/user/name", {
            method: 'POST',
            headers: {
                token: token,
                'content-type': 'application/json'
            },
            body: JSON.stringify({name: name})
        });
    },

    startGame(token) {
        return fetch("http://localhost:8080/games/start", {
            method: 'GET',
            headers: {
                token: token,
            }
        })
            .then((response => response.json()))
    },

    getGameList(token) {
        return fetch("http://localhost:8080/games", {
            method: 'GET',
            headers: {
                token: token
            }
        })
            .then((response => response.json()))
    },

    joinGame(token, gameId) {
        return fetch(`http://localhost:8080/games/join/${gameId}`, {
            method: 'GET',
            headers: {
                token: token,
            }
        })
            .then((response => response.json()))
    },

    getGameStatus(token) {
        return fetch("http://localhost:8080/games/status", {
            method: 'GET',
            headers: {
                token: token
            }
        })
            .then((response => response.json()))
    },

    getMove(token, move){
        return fetch(`http://localhost:8080/games/move?sign=${move}`,{
            method: 'GET',
            headers: {
                token: token,
                'Content-Type': 'application/json'
            }
            })
            .then((response => response.json()))
            .then(res => {return res})
    }
}
export default ApiHandler;

